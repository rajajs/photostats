"""
View statistics stored in sqlite db by photostats

Use flask freeze to generate static pages
"""
# flask and extensions
from flask import Flask, render_template
from flask_frozen import Freezer

from pony.flask import Pony
from pony import orm
# .orm import Database, Required, Optional

from datetime import datetime
import itertools

## init app
app = Flask(__name__)
freezer = Freezer(app)

app.config.update(dict(
    DEBUG = True,
    Pony = {
        'provider': 'sqlite',
        'filename': 'photostats.db',
        'create_db': True
    }    
))


db = orm.Database()
db.bind(**app.config['Pony'])


class Photo(db.Entity):
    filename = orm.Required(str)  # full path. primary identifier

    camera_manufacturer = orm.Optional(str)
    camera_model = orm.Optional(str)
    lens_model = orm.Optional(str, nullable=True)

    datetime_value = orm.Optional(datetime)
    datetime_display = orm.Optional(str)

    focal_length_value = orm.Optional(float)
    focal_length_display = orm.Optional(str)

    iso_value = orm.Optional(int)
    iso_display = orm.Optional(str)

    aperture_value = orm.Optional(float)
    aperture_display = orm.Optional(str)

    shutter_speed_value = orm.Optional(float)
    shutter_speed_display = orm.Optional(str)


db.generate_mapping()
Pony(app)

@app.route('/')
def stats_overview():
    # # homepage
    photos = orm.select(p for p in Photo)[:]
    basic_stats = get_stats(photos)
        
    return render_template('home.html', basic_stats=basic_stats)


@app.route('/<camera_lens>')
def stats(camera_lens):
    """
    camera_lens is string as returned by function camera_lens
    """
    camera_manufacturer, camera_model, lens_model = manufacturer_model(camera_lens) 
    photos = orm.select(p for p in Photo
                        if p.camera_manufacturer == camera_manufacturer and
                        p.camera_model == camera_model and
                        p.lens_model == lens_model)

    basic_stats = get_stats(photos)
    return render_template('home.html', basic_stats=basic_stats)
    
def camera_lens(p):
    """
    String rep of camera manufacturer, model and lens
    input is photo instance
    """
    camera_manufacturer = p.camera_manufacturer or 'Unknown manufacturer'
    camera_model = p.camera_model or 'Unknown model'
    lens_model = p.lens_model or 'Unknown lens'
    
    return ' | '.join([camera_manufacturer, camera_model, lens_model])


def manufacturer_model(camera_lens):
    """
    From string representation of camera lens
    separate manufacturer and model
    """
    camera_manufacturer, camera_model, lens_model = camera_lens.split(' | ')

    if camera_manufacturer == 'Unknown manufacturer':
        camera_manufacturer = None

    if camera_model == 'Unknown model':
        camera_model = None

    if lens_model == 'Unknown lens':
        lens_model = None

    return (camera_manufacturer, camera_model, lens_model)
    

def year_taken(p):
    if p.datetime_value:
        return p.datetime_value.year
    else:
        return None


def get_stats(photos):

    def key_func(tup):
        """
        function for sorting tuple using the first element
        and accounting for None
        """
        if not tup[0]:
            return 0
        else:
            return tup[0]
    

    
    basic_stats = {}
    
    # total photos
    basic_stats['total_photos'] = len(photos)
    
    # photos by year
    photos_by_year = [(year_taken(p), p) for p in photos]
    # key_func = lambda x: x[0]

    basic_stats['photos_by_year'] = []
    photos_by_year.sort(key=key_func)
    for key, group in itertools.groupby(photos_by_year, key_func):
        # print(key, len(list(group)))
        basic_stats['photos_by_year'].append((key, len(list(group))))

    basic_stats['year'] = [y[0] for y in basic_stats['photos_by_year']]
    basic_stats['year_count'] = [y[1] for y in basic_stats['photos_by_year']]        
        
    # photos by camera / lens combo
    photos_by_camera_lens = [(camera_lens(p), p) for p in photos] 

    basic_stats['photos_by_camera_lens'] = []
    photos_by_camera_lens.sort(key = key_func)
    for key, group in itertools.groupby(photos_by_camera_lens, key_func):
        basic_stats['photos_by_camera_lens'].append((key, len(list(group))))

    # sort by number of photos
    basic_stats['photos_by_camera_lens'].sort(key = lambda x: x[1], reverse=True)

    # focal length - pics per focal length / per year also ?
    photos_by_focal_length = [(p.focal_length_value, p) for p in photos]
    basic_stats['photos_by_focal_length'] = []
    photos_by_focal_length.sort(key=key_func)
    for key, group in itertools.groupby(photos_by_focal_length, key_func):
        basic_stats['photos_by_focal_length'].append((key, len(list(group))))    

    # separate labels and values for plotting
    basic_stats['focal_length'] = [fl[0] for fl in basic_stats['photos_by_focal_length']]
    basic_stats['focal_length_count'] = [fl[1] for fl in basic_stats['photos_by_focal_length']] 

    return basic_stats
    

def stats_camera_lens():
    ## for each camera / lens combo
    
    # total photos
    # by focal length (filter by period ?)
    # by aperture
    # by shutter speed

    pass
    

if __name__ == "__main__":
    app.run()
    # freezer.freeze()
