"""
Run tests with 'python3 test_photostats.py'

Run coverage with 'coverage3 run -m --source=. unittest test_photostats.py' 
followed by 'coverage3 report'
"""
import unittest
import os
from pony import orm
from pprint import pprint

from photostats import *
from photodb import *

TEST_DIR = 'photostats_test'
TEST_IMG1 = os.path.join(TEST_DIR, 'test_fuji.jpg')
TEST_IMG2 = os.path.join(TEST_DIR, 'test_canon550.jpg')
TEST_IMG3 = os.path.join(TEST_DIR, 'test_empty_exif.jpg')
TEST_IMG4 = os.path.join(TEST_DIR, 'test_long_exposure.JPG')

db_uri = ":memory:"

class TestPhotoStats(unittest.TestCase):

    def test_get_exif_fuji(self):
        exif = get_exif(TEST_IMG1)
        self.assertEqual(exif['ISOSpeedRatings'], 400)

        
    def test_get_exif_fuji(self):
        exif = get_exif(TEST_IMG2)
        self.assertEqual(exif['ISOSpeedRatings'], 200)

        
    def test_get_exif_empty_exif(self):
        raw_exif = get_exif(TEST_IMG3)
        # refined_exif = refine_exif(raw_exif)        
        self.assertEqual(raw_exif, None)        

        
    def test_refine_exif(self):
        raw_exif = get_exif(TEST_IMG1)
        refined_exif = refine_exif(raw_exif)
        self.assertEqual(refined_exif['datetime_display'], '31-05-2020 10:48:24')
        self.assertEqual(refined_exif['iso_value'], 400)

        
    def test_refine_exif_long_exposure(self):
        raw_exif = get_exif(TEST_IMG4)
        refined_exif = refine_exif(raw_exif)        
        self.assertEqual(refined_exif['shutter_speed_display'], "4.0sec")
        

    def test_find_photos(self):
        img_files = list(find_photos(TEST_DIR))
        self.assertEqual(len(img_files), 26)


    def test_db(self):
        db = create_db(db_uri)

        # test db creation
        with orm.db_session:
            photos = orm.select(p for p in Photo)[:]
            self.assertEqual(photos, [])

        raw_exif = get_exif(TEST_IMG1)
        refined_exif = refine_exif(raw_exif)        

        filename = os.path.abspath(TEST_IMG1)
        refined_exif['filename'] = filename

        # addin a photo
        add_photo(refined_exif)        

        with orm.db_session:
            photo = orm.select(p for p in Photo).first()
            photo_dict = photo.to_dict()
            self.assertDictContainsSubset(refined_exif, photo_dict)   # photo_dict has id in addition
            
            # filename_in_db
            self.assertEqual(filename_in_db(filename), True)
            self.assertEqual(filename_in_db('/not_there'), False)
            
            
if __name__ == "__main__":
    unittest.main()
