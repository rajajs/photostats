"""
Given a folder of pics,
collect non-duplicate images
analyze exif for each and store in sqlite db
allow exploration of data
"""
from PIL import ExifTags, Image

import os
import sys
import time

from datetime import datetime
from pprint import pprint

from photodb import *

# variables
test_folder = '/data/tmp/photostats_test'
ALLOWED_IMG_EXTENSIONS = ['.jpg', '.JPG', '.jpeg', '.JPEG']

## Get exif information for one image
## using PIl
def get_exif(img_file):
    """
    Given path to image file 
    return dict of exif values
    """
    try:
        img = Image.open(img_file)
    except:
        return None

    # if img has not exif info
    if not img._getexif():
        img.close()
        return None

    exif = {
            ExifTags.TAGS[k]: v
            for k, v in img._getexif().items()
            if k in ExifTags.TAGS
        }

    img.close()
    
    return exif


def refine_exif(exif_dict):
    """
    Given the complete exif extracted by PIl
    Lets use only what we need in format we need
    
    for all needed variables, output _value and _display
    
    value is in correct units, allowing comparisons or plotting
    display is as string formatted for displaying 
    """
    exif = {}
    
    exif['camera_manufacturer'] = exif_dict.get('Make', None)
    exif['camera_model'] = exif_dict.get('Model', None)
    exif['lens_model'] = exif_dict.get('LensModel', None)

    if exif['camera_manufacturer']:
        exif['camera_manufacturer'] = exif['camera_manufacturer'].rstrip('\x00')

    if exif['camera_model']:
        exif['camera_model'] = exif['camera_model'].rstrip('\x00')

    if exif['lens_model']:
        exif['lens_model'] = exif['lens_model'].rstrip('\x00')        

    
    if 'DateTimeOriginal' not in exif_dict.keys():
        exif['datetime_value'] = None # datetime(1900, 1, 1)
        exif['datetime_display'] = 'Not known'
    else:
        exif['datetime_value'] = datetime.strptime(exif_dict['DateTimeOriginal'], '%Y:%m:%d %H:%M:%S')
        exif['datetime_display'] = exif['datetime_value'].strftime('%d-%m-%Y %H:%M:%S')

    focal_length = exif_dict.get('FocalLength', None)
    if focal_length:
        exif['focal_length_value'] = focal_length[0] / focal_length[1] 
        exif['focal_length_display'] = str(int(exif['focal_length_value'])) + "mm"  
    else:
        exif['focal_length_value'] = None
        exif['focal_length_display'] = None
        
        
    exif['iso_value'] = exif_dict.get('ISOSpeedRatings', None)
    if exif['iso_value']:
        exif['iso_display'] = 'ISO' + str(exif['iso_value'])
    else:
        exif['iso_display'] = None

    # using 'ApertureValue' gives wrong number, maybe corrected for crop
    aperture = exif_dict.get('FNumber', None)
    if aperture:
        if aperture[1] == 0:
            exif['aperture_value'] = None
            exif['aperture_display'] = None
        else:
            exif['aperture_value'] = aperture[0] / float(aperture[1])
            exif['aperture_display'] = "f {0:4.1f}".format(exif['aperture_value'])
    else:
        exif['aperture_value'] = None
        exif['aperture_display'] = None
        
    shutter_times = exif_dict.get('ExposureTime', None)
    if shutter_times:
        if shutter_times[1] == 0 or shutter_times[0] == 0:
            exif['shutter_speed_value'] = None
            exif['shutter_speed_display'] = None
        else:
            shutter_1, shutter_2 = shutter_times
            exif['shutter_speed_value'] = shutter_1 / float(shutter_2)  # float, in seconds
            if shutter_1 < shutter_2:        
                exif['shutter_speed_display'] = "1/" + str(int(shutter_2 / shutter_1)) + "sec"
            else:
                exif['shutter_speed_display'] = str(shutter_1 / shutter_2) + "sec"
    else:
        exif['shutter_speed_value'] = None
        exif['shutter_speed_display'] = None
        
    # some tweaks
    if exif['camera_model'] and exif['camera_model'].startswith('Canon'):
        exif['camera_model'] = exif['camera_model'][5:]  # remove canon

    return exif    


def find_photos(root):
    """
    Find all photo files
    searching recursively given path to root dir
    returns full path to image file if extension is in ALLOWED_IMG_EXTENSIONS
    Generator so that each filename can be handled in memory efficient manner
    """
    for dirpath, _, filenames in os.walk(root):
        for filename in filenames:
            if os.path.splitext(filename)[1] in ALLOWED_IMG_EXTENSIONS:
                yield os.path.join(dirpath, filename)


if __name__ == "__main__":
    # first argument is path to root
    if len(sys.argv) != 2:
        print('Please call with path to root directory')
        sys.exit()

    root = sys.argv[1]
    db_uri = os.path.join(root, 'photostats.db')
    db = create_db(db_uri)
    
    start_time = time.time()
    new_photos = 0
    indexed_photos = 0
    
    photos = find_photos(root)

    for filename in photos:
        print('-------------')
        print('starting ', filename)
        if not filename_in_db(filename):
            new_photos += 1
            print('new photo')
            raw_exif = get_exif(filename)

            if raw_exif:
                refined_exif = refine_exif(raw_exif)
                refined_exif['filename'] = filename

                print('adding ')
                add_photo(refined_exif)

            else:
                print('No exif info')

        else:
            indexed_photos += 1
            print('already exists')

    elapsed_time = time.time() - start_time
    print('------------------------')
    print('Indexed {} new photos. Verified {} old photos. Took {:.2f} seconds'.\
          format(new_photos, indexed_photos, elapsed_time))
