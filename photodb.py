"""
Provide database related functions.
Use sqlite db stored in root dir to store the data
use pony orm 
"""

from pony import orm
from datetime import datetime

db = orm.Database()

def create_db(db_uri):
    db.bind(provider='sqlite', filename=db_uri, create_db=True)
    db.generate_mapping(create_tables=True)
    return db

class Photo(db.Entity):
    filename = orm.Required(str)  # full path. primary identifier

    camera_manufacturer = orm.Optional(str, nullable=True)
    camera_model = orm.Optional(str, nullable=True)
    lens_model = orm.Optional(str, nullable=True)

    datetime_value = orm.Optional(datetime)
    datetime_display = orm.Optional(str, nullable=True)

    focal_length_value = orm.Optional(float)
    focal_length_display = orm.Optional(str, nullable=True)

    iso_value = orm.Optional(int)
    iso_display = orm.Optional(str, nullable=True)

    aperture_value = orm.Optional(float)
    aperture_display = orm.Optional(str, nullable=True)

    shutter_speed_value = orm.Optional(float)
    shutter_speed_display = orm.Optional(str, nullable=True)

    
def add_photo(photo_dict):
    with orm.db_session:
        photo = Photo(
            filename = photo_dict['filename'],
            camera_model = photo_dict['camera_model'],
            camera_manufacturer = photo_dict['camera_manufacturer'],
            lens_model = photo_dict['lens_model'],
            datetime_value = photo_dict['datetime_value'],
            datetime_display = photo_dict['datetime_display'],
            focal_length_value = photo_dict['focal_length_value'],
            focal_length_display = photo_dict['focal_length_display'],
            iso_value = photo_dict['iso_value'],
            iso_display = photo_dict['iso_display'],
            aperture_value = photo_dict['aperture_value'],
            aperture_display = photo_dict['aperture_display'],
            shutter_speed_value = photo_dict['shutter_speed_value'],
            shutter_speed_display = photo_dict['shutter_speed_display'],
        )
        
    orm.commit()


def filename_in_db(filename):
    """
    Is this filename already stored in the db
    """
    with orm.db_session:
        exists = orm.select(p for p in Photo if p.filename == filename).first()
        
    if exists:
        return True
    else:
        return False
