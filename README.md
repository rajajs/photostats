Photostats
----------

photostats.py - Given a root directory, lists all photos including everything in subfolders, collects exif information using PIl and writes everything to an sqlite database named 'photostats.db' in the root directory.

photostats_view.py - With the sqlite db in the same folder as the script, this runs a server on localhost port 5000 displaying the statistics for the photos (photos per year, photos per camera/lens combo, photos at each aperture and so on).

Written for my use, the code is quickly put together and ugly. Not fit for general use by everyone.

Typical workflow -

1. In a virtualenv
   `pip3 install -r requirements.txt`

2. `python3 photostats.py /path/to/root/directory`

3. Copy 'photostats.db' from root directory to .

4. `python photostats_view.py`

